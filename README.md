# peerreview.word-b.com

#SETUP

#my.cnf file in the the root of peerreview app structured as follow:

#[client]

#database = DBNAME

#user = USER

#password = PWD

#default-character-set = utf8

#Mysql >5 suggested

#local_settings.py in the same directory of settings.py (/peerreview/peerreview)

#EMAIL_HOST =

#EMAIL_HOST_USER = 

#EMAIL_HOST_PASSWORD = 

#EMAIL_PORT = 

#EMAIL_USE_TLS = 

#DEFAULT_FROM_EMAIL =


#This project is an attempt to process long text with topic modelling and document clustering. It is divided in two repo, this is the one for back end, structured with a django rest api to handle texts

#The project is still in the phase of analysis but anyone can give a hand asking to be included 

#in the repo and in the JIRA at the address: agnese.camellini@gmail.com

#The project is Open Source, published with GNU General Public License version 2 
#(so you can use it even for commercial purposes). The project comes with NO WARRANTY of being suited for 

#whichever use, and it is still in the first movements. The deadline to release it is 01/06/2020 in the 

#timezone of Rome.

#INSTRUCTIONS TO MANAGE DEPENDENCIES

#FEDORA LINUX EXAMPLE INSTALL

#sudo dnf install pipenv

#Go to the directory of the repo:

#pipenv --three

#pipenv install --dev

#This project is optimized for Unix systems and, due to some dependencie's issues, it does not run on windows.

#to run django type:

#pipenv shell

#in the root directory

#cd peerreview

#python3 manage.py runserver

