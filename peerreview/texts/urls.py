"""texts URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from . import views
from rest_framework import routers

"""CUSTOM ENDPOINTS"""
list_username = views.UserView.as_view({
    'get': 'list_username'
})

list_login = views.UserView.as_view({
    'post': 'list_login'
})

list_access = views.UserView.as_view({
    'get': 'list_access'
})

token_confirm = views.UserView.as_view({
    'get': 'token_confirm'
})

token_cng_pwd = views.UserView.as_view({
    'get': 'token_cng_pwd'
})

get_by_email = views.UserView.as_view({
    'get': 'get_by_email'
})

document_by_user= views.DocumentView.as_view({
    'get': 'document_by_user'
})

anonimous_document = views.DocumentView.as_view({
    'get':'anonimous_document'
})

revision_by_document = views.RevisionView.as_view({
    'get':'revision_by_document'
})

category_by_revision = views.RevisionCategoryView.as_view({
    'get':'category_by_revision'
})

categories_by_user = views.UserCategoryView.as_view({
    'get': 'categories_by_user'
})

generate_by_revision = views.RevisionCategoryView.as_view({
    'get': 'generate_by_revision'
})

resend_activation_email = views.UserView.as_view({
    'put': 'resend_activation_email'
})
"""routers"""
router = routers.DefaultRouter()
router.register('user', views.UserView)
router.register('document', views.DocumentView)
router.register('revisions', views.RevisionView)
router.register('category', views.CategoryView)
router.register('revision-category', views.RevisionCategoryView)
router.register('user-category', views.UserCategoryView)
router.register('files', views.FilesView)
        
"""Everything together"""
urlpatterns =[
    path('api/v3/', include(router.urls)),
    path('api/v3/user/list_username/{username}/', list_username, name= 'list_username'),
    path('api/v3/user/list_login/', list_login, name='list_login'),
    path('api/v3/user/token_confirm/{token_confirm}/', token_confirm, name ='token_confirm'),
    path('api/v3/user/token_cng_pwd/{token_cng_pwd}/', token_cng_pwd, name = 'token_cng_pwd'),
    path('api/v3/user/get_by_email/{email}', get_by_email, name='get_by_email'),
    path('api/v3/user/resend_activation_email/', resend_activation_email, name='resend_activation_email'),
    path('api/v3/user/list_access/', list_access, name='list_access'),
    path('api/v3/document/document_by_user/{user_id}', document_by_user, name = 'document_by_user'),
    path('api/v3/document/anonimous_document/{user_id}/{name}', anonimous_document, name = 'anonimous_document'),
    path('api/v3/revisions/revision_by_document/{document_id}', revision_by_document, name= 'revision_by_document'),
    path('api/v3/revision-category/category_by_revision/{revision_id}', category_by_revision, name = 'category_by_revision'),
    path('api/v3/user-category/categories_by_user/{user_id}/', categories_by_user, name = 'categories_by_user'),
    path('api/v3/revision-category/generate_by_revision/{revision_id}', generate_by_revision, name = 'generate_by_revision')
    ]
