from django.db import models
import jsonfield

class User(models.Model):
    id = models.AutoField(primary_key=True, db_column = 'id')
    name = models.CharField(max_length = 250, db_column = 'name')
    username = models.CharField(max_length = 250, db_column = 'username')
    pwd = models.CharField(max_length = 500, db_column = 'pwd')
    email = models.CharField(max_length = 500, db_column = 'email')
    access_token = models.CharField(max_length = 1000, db_column = 'access_token', null = True)
    token_confirm = models.CharField(max_length = 1000, db_column = 'token_confirm')
    token_cng_pwd = models.CharField(max_length = 1000, db_column = 'token_cng_pwd', null = True)
    flg_confirm = models.BooleanField(db_column = 'flg_confirm')

    class Meta:
        db_table = 'T_USERS'
        constraints = [
            models.UniqueConstraint(fields=['username', 'pwd'], name='unique_user')
        ]


    def __str__(self):
        return self.name

class Document(models.Model):
    id = models.AutoField(primary_key=True, db_column = 'id')
    name = models.CharField(max_length = 500, db_column = 'name')
    user_id = models.ForeignKey('User', on_delete=models.DO_NOTHING, db_column = 'user_id')

    class Meta:
        db_table = 'T_DOCUMENTS'

    def __str__(self):
        return self.name

class Revision(models.Model):
    id = models.AutoField(primary_key=True, db_column = 'id')
    document_id = models.ForeignKey('Document', on_delete=models.DO_NOTHING, db_column = 'document_id')
    number = models.IntegerField(db_column ='number')
    content = models.TextField(db_column = 'content', null = True)
    outline = jsonfield.JSONField(db_column = 'outline', null = True)

    class Meta:
        db_table = 'T_REVISIONS'
        constraints = [
            models.UniqueConstraint(fields=['document_id', 'number'], name='unique_revision')
        ]

    def __str__(self):
        return self.document_id.name + str(self.number)

class Category(models.Model):
    id = models.AutoField(primary_key=True, db_column = 'id')
    name = models.CharField(max_length=150, db_column = 'name')

    class Meta:
        db_table = 'T_CATEGORIES'

    def __str__(self):
        return self.name

class RevisionCategory(models.Model):
    id = models.AutoField(primary_key=True, db_column = 'id')
    revision = models.ForeignKey(Revision, on_delete=models.DO_NOTHING, db_column = 'revision')
    category_id = models.ForeignKey(Category, on_delete=models.DO_NOTHING, db_column = 'category_id')

    class Meta:
        db_table = 'T_REVISIONS_CATEGORIES'
        constraints = [
            models.UniqueConstraint(fields=['revision', 'category_id'], name='unique_revision_category')
        ]

    def __str__(self):
        return self.revision.document_id.name + ' - ' + self.category_id.name

class UserCategory(models.Model):
    id = models.AutoField(primary_key=True, db_column = 'id')
    user_id = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column = 'user_id')
    category_id = models.ForeignKey(Category, on_delete=models.DO_NOTHING, db_column = 'category_id')

    class Meta:
        db_table = 'T_USERS_CATEGORIES'
        constraints = [
            models.UniqueConstraint(fields=['user_id', 'category_id'], name='unique_ user_category')
        ]

    def __str__(self):
        return self.user_id.name + ' - ' +self.category_id.name

class Files(models.Model):
    id = models.AutoField(primary_key =True, db_column = 'id')
    title = models.CharField(max_length=500, db_column = 'title')
    file = models.FileField(upload_to='documents', max_length = 4000, db_column ='file')
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='user_id')
    document = models.ForeignKey(Document, on_delete=models.DO_NOTHING, db_column='document_id')
    revision = models.ForeignKey(Revision, on_delete=models.DO_NOTHING, db_column='revision_id')
    
    class Meta:
        db_table = 'T_FILES'

    def __str__(self):
        return self.title + ' - ' + self.user.username + ' - ' + self.document.name
