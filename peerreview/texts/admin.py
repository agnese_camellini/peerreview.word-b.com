from django.contrib import admin
from .models import User, Document, Revision, Category, RevisionCategory, UserCategory

admin.site.register(User)
admin.site.register(Document)
admin.site.register(Revision)
admin.site.register(Category)
admin.site.register(RevisionCategory)
admin.site.register(UserCategory)

