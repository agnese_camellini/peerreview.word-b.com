from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from rest_framework import viewsets
from .models import User, Document, Revision, Category, RevisionCategory, UserCategory, Files
from .serializers import UserSerializer, DocumentSerializer, RevisionSerializer, CategorySerializer, RevisionCategorySerializer, UserCategorySerializer, FilesSerializer, RevisionCategoryNestedSerializer, UserCategoryNestedSerializer, RevisionCategorySemiNestedSerializer
from django.core.mail import send_mail
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from .document_data_extractor import *
from django.db.models import Max
from .argument_extractor import *
from functools import wraps
from django.db.models import QuerySet
from rest_framework.exceptions import PermissionDenied
import os
import json

"""CUSTOM FILTERS"""
class MultipleFieldLookupMixin(object):
    """
    Apply this mixin to any view or viewset to get multiple field filtering
    based on a `lookup_fields` attribute, instead of the default single field filtering.
    """
    def get_object(self):
        queryset = self.get_queryset()             # Get the base queryset
        queryset = self.filter_queryset(queryset)  # Apply any filter backends
        f = {}
        for field in self.lookup_fields:
            if field in self.kwargs.keys() and self.kwargs[field] != 'None' and self.kwargs[field] is not None : # Ignore empty fields.
                f[field] = self.kwargs[field]
        obj = get_object_or_404(queryset, **f)  # Lookup the object
        return obj

"""Custom Pagination"""
class StandardResultsSetPagination(PageNumberPagination):
    page_size = 50
    page_size_query_param = 'page_size'
    max_page_size = 500

"""Pagination for custom action"""

def paginate(func):

    @wraps(func)
    def inner(self, *args, **kwargs):
        queryset = func(self, *args, **kwargs)
        assert isinstance(queryset, (list, QuerySet)), "apply_pagination expects a List or a QuerySet"

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
    return inner


class UserView(MultipleFieldLookupMixin, viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    lookup_fields=['pk', 'username', 'pwd', 'email', 'token_confirm', 'token_cng_pwd']

    def create(self, request):
        anonymous_user = User.objects.filter(username = 'anonymous')
        checkAccessToken(request, anonymous_user[0].id)
        confirmation_link = 'http://word-b.com/#/account_confirmation/?token_confirm='+request.data['token_confirm']
        email = request.data['email']
        send_mail('Confirmation of the registration to word-b.com','To confirm your registration with username "'+request.data['username']+'" paste this link to the browser header: '+confirmation_link,'no-replay@word-b.com',[email],fail_silently=False)
        response = super(UserView, self).create(request)
        return response

    def update(self, request, pk):
        if(pk != "resend_activation_email"):
            checkAccessToken(request, pk)
            if(request.data['pwd']== '$$word-b-pwd-changing$$'):
                confirmation_link = 'http://word-b.com/#/pwd_confirmation?token_cng_pwd='+request.data['token_cng_pwd']
                email = request.data['email']
                send_mail('Change password email from word-b.com','To change the password for the username "'+ request.data['username']+'" paste this link to the browser header: '+confirmation_link,'no-replay@word-b.com',[email],fail_silently=False)
                user = User.objects.get(pk = pk)
                serializer = UserSerializer(user)
                return Response(serializer.data)
            else:
                response = super(UserView, self).update(request)
            return response
        else:
            self.resend_activation_email(request)
            user = User.objects.get(pk = request.data['id'])
            serializer = UserSerializer(user)
            return Response(serializer.data)

    def destroy(self, request, pk):
        checkAccessToken(request, pk)
        documents = Document.objects.filter(user_id = pk)
        for document in documents:
            revisions = Revision.objects.filter(document_id = document.id)
            for revision in revisions:
                Files.objects.filter(revision = revision.id).delete()
                RevisionCategory.objects.filter(revision = revision.id).delete()
                revision.delete()
            Files.objects.filter(document = document.id) .delete()
            document.delete()
        UserCategory.objects.filter(user_id = pk).delete()
        super(UserView, self).destroy(request)
        return Response(data = 'success')

    def list(self, request):
        checkAccessTokenAnonymous(request)
        return self.queryset

    def retrieve(self, request, pk):
        checkAccessTokenAnonymous(request)
        return super(UserView, self).retrieve(request)
    
    @action(detail=False, methods = ['get'], serializer_class=UserSerializer, url_name='list_username')
    def list_username(self, request, pk = None, *args, **kwargs):
        pk = None
        username = request.query_params.get('username')
        if username != 'anonymous':
            checkAccessTokenAnonymous(request)
        users = User.objects.filter(username=username)
        if not users:
            users = User.objects.filter(username="anonymous")
            serializer = self.get_serializer(users[0], many = False)
            return Response(data = json.loads('{"bool" : false, "user" :' + json.dumps(serializer.data)+ '}'))
        else:
            serializer = self.get_serializer(users[0], many = False)
            return Response(data= json.loads('{"bool" : true, "user" :' + json.dumps(serializer.data)+ '}'))

    @action(detail=False, methods =['post'], serializer_class = UserSerializer, url_name='list_login')
    def list_login(self, request, pk = None, *args, **kwargs):
        pk = None
        checkAccessTokenAnonymous(request)
        username = request.data.get('username')
        pwd = request.data.get('pwd')
        users = User.objects.filter(username = username, pwd = pwd)
        serializer = self.get_serializer(users, many = True)
        return Response(serializer.data)

    @action(detail=False, methods = ['get'], serializer_class = UserSerializer, url_name='token_confirm')
    def token_confirm(self, request, pk = None, *args, **kwargs):
        pk = None
        checkAccessTokenAnonymous(request)
        token_confirm = request.query_params.get('token_confirm')
        users = User.objects.filter(token_confirm = token_confirm)
        if not users:
            users = User.objects.filter(username="anonymous")
            serializer = self.get_serializer(users[0], many = False)
            return Response(data = json.loads('{"bool" : false, "user" :' + json.dumps(serializer.data)+ '}'))
        else:
            serializer = self.get_serializer(users[0], many = False)
            return Response(data= json.loads('{"bool" : true, "user" :' + json.dumps(serializer.data)+ '}'))
    
    @action(detail=False, methods=['get'], serializer_class = UserSerializer, url_name = 'token_cng_pwd')
    def token_cng_pwd(self, request, pk = None, *args, **kwargs):
        pk = None
        checkAccessTokenAnonymous(request)
        token_cng_pwd = request.query_params.get('token_cng_pwd')
        users = User.objects.filter(token_cng_pwd = token_cng_pwd)
        if not users:
            users = User.objects.filter(username="anonymous")
            serializer = self.get_serializer(users[0], many = False)
            return Response(data = json.loads('{"bool" : false, "user" :' + json.dumps(serializer.data)+ '}'))
        else:
            serializer = self.get_serializer(users[0], many = False)
            return Response(data= json.loads('{"bool" : true, "user" :' + json.dumps(serializer.data)+ '}'))

    @action(detail=False, methods=['get'], serializer_class = UserSerializer, url_name='get_by_email')
    def get_by_email(self, request, pk=None, *args, **kwargs):
        pk = None
        checkAccessTokenAnonymous(request)
        email = request.query_params.get('email')
        users = User.objects.filter(email = email)
        if not users:
            users = User.objects.filter(username="anonymous")
            serializer = self.get_serializer(users[0], many = False)
            return Response(data = json.loads('{"bool" : false, "user" :' + json.dumps(serializer.data)+ '}'))
        else:
            serializer = self.get_serializer(users[0], many = False)
            return Response(data= json.loads('{"bool" : true, "user" :' + json.dumps(serializer.data)+ '}'))

    def resend_activation_email(self, request, pk = None, *args, **kwargs):
        pk=None
        checkAccessTokenAnonymous(request)
        user = User.objects.get(pk= request.data['id'])
        confirmation_link = 'http://word-b.com/#/account_confirmation/?token_confirm='+request.data['token_confirm']
        email = request.data['email']
        send_mail('Confirmation of the registration to word-b.com','To confirm your registration with username "'+request.data['username']+'" paste this link to the browser header: '+confirmation_link,'no-replay@word-b.com',[email],fail_silently=False)

    @action(detail=False, methods = ['get'], serializer_class=UserSerializer, url_name='list_access')
    def list_access(self, request, pk = None, *args, **kwargs):
        pk = None
        checkAccessTokenAnonymous(request)
        at = request.query_params.get('access')
        users = User.objects.filter(access_token=at)
        if not users:
            users = User.objects.filter(username="anonymous")
            serializer = self.get_serializer(users[0], many = False)
            return Response(data = json.loads('{"bool" : false, "user" :' + json.dumps(serializer.data)+ '}'))
        else:
            serializer = self.get_serializer(users[0], many = False)
            return Response(data= json.loads('{"bool" : true, "user" :' + json.dumps(serializer.data)+ '}'))

class DocumentView(MultipleFieldLookupMixin, viewsets.ModelViewSet):
    queryset = Document.objects.all()
    serializer_class = DocumentSerializer
    lookup_fields= ['pk', 'name', 'user_id']

    def create(self, request):
        checkAccessTokenAnonymous(request)
        return super(DocumentView, self).create(request)

    def update(self, request, pk):
        checkAccessTokenAnonymous(request)
        return super(DocumentView, self).update(request)

    def retrieve(self, request, pk):
        checkAccessTokenAnonymous(request)
        return super(DocumentView, self).retrieve(request)

    def destroy(self, request, pk):
        #print("deleting document:"+ pk)
        document = Document.objects.get(pk=pk)
        checkAccessToken(request, document.user_id.id)
        revisions = Revision.objects.filter(document_id = pk)
        for revision in revisions:
            Files.objects.filter(revision = revision.id).delete()
            RevisionCategory.objects.filter(revision = revision.id).delete()
            revision.delete()
        #print("deleted revisions connected")
        super(DocumentView, self).destroy(request)
        return Response(data = 'success')

    def list(self, request):
        checkAccessTokenAnonymous(request)
        return self.queryset

    @action(detail=False, methods=['get'], serializer_class = DocumentSerializer, url_name = 'document_by_user')
    def document_by_user(self, request, pk=None, *args, **kwargs):
        pk = None
        user_id = request.query_params.get('user_id')
        checkAccessToken(request, user_id)
        user = User.objects.get(pk=user_id)
        documents = Document.objects.filter(user_id=user)
        serializer = self.get_serializer(documents, many=True)
        return Response(serializer.data)

    @action(detail=False, methods=['get'], serializer_class = DocumentSerializer, url_name='anonimous_document')
    def anonimous_document(self, request, pk= None, *args, **kwargs):
        pk = None
        users = User.objects.filter(username="anonymous")
        checkAccessTokenAnonymous(request)
        name = request.query_params.get('name')
        documents = Document.objects.filter(user_id=users[0], name = name)
        serializer = self.get_serializer(documents, many=True)
        return Response(serializer.data)

class RevisionView(MultipleFieldLookupMixin, viewsets.ModelViewSet):
    queryset = Revision.objects.all()
    serializer_class = RevisionSerializer
    lookup_fields = ['pk', 'document_id']

    def create(self, request):
        revision = Revision()
        document = Document.objects.get(pk=request.data['document_id'])
        checkAccessToken(request, document.user_id.id)
        revision.document_id=document
        revisions = Revision.objects.filter(document_id = document)
        if not revisions:
            revision.number = 1
        else:
            revision.number = revisions.aggregate(Max('number'))['number__max']+1
        revision.save()
        serializer = self.get_serializer(revision, many=False)
        return Response(serializer.data)
    
    def list(self, request):
        checkAccessTokenAnonymous(request)
        return self.queryset

    def update(self, request, pk):
        checkAccessTokenAnonymous(request)
        return super(RevisionView, self).update(request)

    def retrieve(self, request, pk):
        checkAccessTokenAnonymous(request)
        return super(RevisionView, self).retrieve(request)

    def destroy(self, request, pk):
        checkAccessTokenAnonymous(request)
        return super(RevisionView, self).destroy(request)


    @action(detail=False, methods=['get'], serializer_class = RevisionSerializer, url_name='revision_by_document')
    def revision_by_document(self, request, pk = None, *args, **kwargs):
        pk = None
        document_id = request.query_params.get('document_id')
        document = Document.objects.get(pk=document_id)
        print("revision_by_document"+document.user_id.username)
        if(document.user_id.username!="anonymous"):
            checkAccessToken(request, document.user_id.id)
        else:
            checkAccessTokenAnonymous(request)
        revisions = Revision.objects.filter(document_id = document).order_by('-number')
        serializer = self.get_serializer(revisions, many=True)
        return Response(serializer.data)

class CategoryView(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    def list(self, request):
        checkAccessTokenAnonymous(request)
        return self.queryset

    def create(self, request, pk):
        checkAccessTokenAnonymous(request)
        return super(CategoryView, self).create(request)

    def update(self, request, pk):
        checkAccessTokenAnonymous(request)
        return super(CategoryView, self).update(request)

    def retrieve(self, request, pk):
        checkAccessTokenAnonymous(request)
        return super(CategoryView, self).retrieve(request)

    def destroy(self, request, pk):
        checkAccessTokenAnonymous(request)
        return super(CategoryView, self).destroy(request)

class RevisionCategoryView(MultipleFieldLookupMixin, viewsets.ModelViewSet):
    queryset = RevisionCategory.objects.all()
    serializer_class = RevisionCategorySerializer
    lookup_fields = ['pk', 'revision']

    def list(self, request):
        checkAccessTokenAnonymous(request)
        return self.queryset
    
    def create(self, request, pk):
        checkAccessTokenAnonymous(request)
        return super(RevisionCategoryView, self).create(request)

    def update(self, request, pk):
        checkAccessTokenAnonymous(request)
        return super(RevisionCategoryView, self).update(request)

    def retrieve(self, request, pk):
        checkAccessTokenAnonymous(request)
        return super(RevisionCategoryView, self).retrieve(request)

    def destroy(self, request, pk):
        checkAccessTokenAnonymous(request)
        return super(RevisionCategoryView, self).destroy(request)

    @action(detail = False, methods = ['get'], serializer_class = RevisionCategorySemiNestedSerializer, url_name='category_by_revision')
    def category_by_revision(self, request, pk=None, *args, **kwargs):
        pk = None
        revision_id = request.query_params.get('revision_id')
        revision = Revision.objects.get(pk=revision_id)
        checkAccessToken(request, revision.document_id.user_id.id)
        revisionCategories = RevisionCategory.objects.filter(revision = revision).order_by('category_id__name')
        serializer = self.get_serializer(revisionCategories, many= True)
        return Response(serializer.data)

    @action(detail = False, methods = ['get'], serializer_class = RevisionCategoryNestedSerializer, url_name='generate_by_revision')
    def generate_by_revision(self, request, pk=None, *args, **kwargs):
        pk = None
        revision_id = request.query_params.get('revision_id')
        revision = Revision.objects.get(pk = revision_id)
        user = revision.document_id.user_id
        checkAccessToken(request, user.id)
        sample_text = revision.content.replace('\n', ' ')
        extractor = ElaborateArguments()
        try:
            arguments_list = extractor.process_content(sample_text)
            returnList = []
            for i in arguments_list:
                if not Category.objects.filter(name = i):
                    cat = Category()
                    cat.name = i
                    cat.save()
                else:
                    cat = Category.objects.filter(name = i)[0]
                if not UserCategory.objects.filter(user_id = user, category_id = cat):
                    userCategory = UserCategory()
                    userCategory.user_id = user
                    userCategory.category_id = cat
                    userCategory.save()
                if not RevisionCategory.objects.filter(revision = revision, category_id= cat):
                    toBeListedObject = RevisionCategory()
                    toBeListedObject.revision = revision
                    toBeListedObject.category_id = cat
                    toBeListedObject.save()
                else:
                    toBeListedObject = RevisionCategory.objects.filter(revision = revision, category_id= cat)[0]
                alreadyPresent = 0
                for i in returnList:
                    if i == toBeListedObject:
                        alreadyPresent = 1
                if alreadyPresent == 0:
                    returnList.append(toBeListedObject)
            serializer = self.get_serializer(returnList, many=True)
            return Response(serializer.data)
        except:
            return Response(data= json.loads('{"bool" : false}'))
        
        


class UserCategoryView(MultipleFieldLookupMixin, viewsets.ModelViewSet):
    queryset = UserCategory.objects.all()
    serializer_class = UserCategorySerializer
    pagination_class = StandardResultsSetPagination 
    lookup_fields = ['pk', 'user_id']

    def destroy(self, request, pk):
        user_category = UserCategory.objects.get(pk = pk)
        checkAccessToken(request, user_category.user_id.id)
        print("deleting categoryRevision connection:"+ pk)
        super(UserCategoryView, self).destroy(request)
        return Response(data = 'success')

    def list(self, request):
        checkAccessTokenAnonymous(request)
        return self.queryset

    def create(self, request, pk):
        checkAccessTokenAnonymous(request)
        return super(UserCategoryView, self).create(request)

    def update(self, request, pk):
        checkAccessTokenAnonymous(request)
        return super(UserCategoryView, self).update(request)

    def retrieve(self, request, pk):
        checkAccessTokenAnonymous(request)
        return super(UserCategoryView, self).retrieve(request)
    
    @paginate
    @action(detail = False, methods = ['get'], serializer_class = UserCategoryNestedSerializer, url_name='categories_by_user')
    def categories_by_user(self, request, pk = None, *args, **kwargs):
        pk = None
        user_id = request.query_params.get('user_id')
        checkAccessToken(request, user_id)
        user = User.objects.get(pk=user_id)
        #categories = UserCategory.objects.filter(user_id = user).order_by('category_id__name')
        #serializer = self.get_serializer(categories, many=True)
        return UserCategory.objects.filter(user_id = user).order_by('category_id__name')

class FilesView(viewsets.ModelViewSet):
    queryset = Files.objects.all()
    serializer_class = FilesSerializer

    def create(self, request):
        user = User.objects.get(pk=request.data['user'])
        checkAccessToken(request, user.id)
        response = super(FilesView, self).create(request)
        file_id = response.data['id']
        fileSaved = Files.objects.get(pk=file_id)
        revision = fileSaved.revision
        filePath = fileSaved.file.path
        document = DocumentDataExtractor.extract_document_data(filePath)
        revision.outline = document.toc_as_json()
        revision.content = document.document_text
        revision.save()
        return response

    def destroy(self, request, pk=None):
        fileSaved = Files.objects.get(pk = pk)
        checkAccessToken(request, fileSaved.user.id)
        if os.path.isfile(fileSaved.file.path):
            os.remove(fileSaved.file.path)
        response = super(FilesView, self).destroy(request)
        return response
    
    def list(self, request):
        checkAccessTokenAnonymous(request)
        return self.queryset
    
    def update(self, request, pk):
        checkAccessTokenAnonymous(request)
        return super(FilesView, self).update(request)

    def retrieve(self, request, pk):
        checkAccessTokenAnonymous(request)
        return super(FilesView, self).retrieve(request)


def checkAccessToken(request, user_id):
    if "HTTP_ACCESS" in  request.META:
        users = User.objects.filter(access_token = request.META["HTTP_ACCESS"])
        print("USER BY ACCESS"+str(users[0].id))
        print("USER PASSED"+ str(user_id))
        if not users or str(user_id) != str(users[0].id):
            raise PermissionDenied()
    else:
        raise PermissionDenied()

def checkAccessTokenAnonymous(request):
    if "HTTP_ACCESS" in  request.META:
        users = User.objects.filter(access_token = request.META["HTTP_ACCESS"])
        if not users:
            raise PermissionDenied()
    else:
        raise PermissionDenied()
