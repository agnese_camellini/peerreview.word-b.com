from rest_framework import serializers
from .models import User, Document, Revision, Category, RevisionCategory, UserCategory, Files

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields=('id','name', 'username', 'pwd', 'email', 'access_token', 'token_confirm', 'token_cng_pwd', 'flg_confirm')

class UserCustomSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields=('id', 'name')

class DocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        fields = ('id','name','user_id')

class RevisionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Revision
        fields = ('id','document_id', 'number', 'outline', 'content')

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name')

class RevisionCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = RevisionCategory
        fields = ('id', 'revision', 'category_id')

class RevisionCategoryNestedSerializer(RevisionCategorySerializer):
    revision = RevisionSerializer(read_only=True)
    category_id = CategorySerializer(read_only=True)

class RevisionCategorySemiNestedSerializer(RevisionCategorySerializer):
    category_id = CategorySerializer(read_only = True)

class UserCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = UserCategory
        fields = ('id', 'user_id', 'category_id')
    
class UserCategoryNestedSerializer(UserCategorySerializer):
    user_id = UserCustomSerializer(read_only = True)
    category_id = CategorySerializer(read_only = True)

class FilesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Files
        fields = ('id', 'title', 'file', 'user', 'document', 'revision')
