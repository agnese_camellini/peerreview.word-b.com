CREATE TABLE T_USERS(
    id serial NOT NULL PRIMARY KEY,
    name varchar(250) NOT NULL,
    username varchar(250) NOT NULL,
    pwd varchar(500) NOT NULL,
    email varchar(500) NOT NULL,
    access_token varchar(1000) NULL,
    token_confirm varchar(1000) NOT NULL,
    token_cng_pwd varchar(1000) NULL,
    flg_confirm boolean NOT NULL,
    UNIQUE unique_user(username, pwd)
);

CREATE TABLE T_DOCUMENTS(
	id serial NOT NULL PRIMARY KEY,
	name varchar(500) NOT NULL,
	user_id bigint(20)unsigned NOT NULL,
	CONSTRAINT fk_user_document FOREIGN KEY (user_id) REFERENCES T_USERS(id)
);

CREATE TABLE T_REVISIONS(
	id serial NOT NULL PRIMARY KEY,
	document_id bigint(20)unsigned NOT NULL,
	number int(11) NOT NULL,
	content longtext NULL,
	outline JSON DEFAULT NULL,
	FOREIGN KEY (document_id) REFERENCES T_DOCUMENTS(id),
	UNIQUE unique_revision(document_id, number)
);

CREATE TABLE T_CATEGORIES(
	id serial NOT NULL PRIMARY KEY,
	name varchar(150)
);

CREATE TABLE T_REVISIONS_CATEGORIES(
	id serial NOT NULL PRIMARY KEY,
	revision bigint(20)unsigned NOT NULL,
	category_id bigint(20)unsigned NOT NULL,
	FOREIGN KEY (revision) REFERENCES T_REVISIONS(id),
    Foreign key (category_id) references T_CATEGORIES(id),
	UNIQUE unique_revision_category(revision, category_id)
);

CREATE TABLE T_USERS_CATEGORIES(
	id serial NOT NULL PRIMARY KEY,
	user_id bigint(20)unsigned NOT NULL,
	category_id bigint(20)unsigned NOT NULL,
	FOREIGN KEY (user_id) REFERENCES T_USERS(id),
	FOREIGN KEY (category_id) REFERENCES T_CATEGORIES(id),
	UNIQUE unique_user_category(user_id, category_id)
);

 CREATE TABLE T_FILES(
	id serial NOT NULL PRIMARY KEY,
    title varchar(500) not null,
    file varchar(4000) not null,
    user_id bigint(20) unsigned not null,
    document_id bigint(20) unsigned not null,
    revision_id bigint(20) unsigned null,
    CONSTRAINT FK_FILES_USER FOREIGN KEY (user_id) REFERENCES T_USERS(id),
    CONSTRAINT FK_FILES_DOCUMENT FOREIGN KEY (document_id) REFERENCES T_DOCUMENTS(id),
	CONSTRAINT FK_FILES_REVISION FOREIGN KEY (revision_id) REFERENCES T_REVISIONS(id)
 );
