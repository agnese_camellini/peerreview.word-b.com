from nltk.corpus import PlaintextCorpusReader
from nltk.tokenize import PunktSentenceTokenizer
import nltk
from nltk.tree import *

class ElaborateArguments():

	def __init__(self):
		corpus_root = '/home/joy/peerreview.word-b.com/static'
		wordlists = PlaintextCorpusReader(corpus_root, 'bawe_corpus.*')
		self.custom_sent_tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
		#self.custom_sent_tokenizer = PunktSentenceTokenizer(wordlists)

	def process_content(self, sample_text):
		try:
			namedEnt = []
			tokenized = self.custom_sent_tokenizer.tokenize(sample_text)		
			for i in tokenized:
				words = nltk.word_tokenize(i)
				tagged = nltk.pos_tag(words)
				chunked= nltk.ne_chunk(tagged, binary=False)
				continuous_chunk = []
				current_chunk = []
				for i in chunked:
					if type(i) == Tree:
						for token, pos in i.leaves():
							if pos == 'NN' or pos == 'NNP':
								current_chunk.append(" ".join([token]))
					elif current_chunk:
						named_entity = " ".join(current_chunk)
						if named_entity not in continuous_chunk:
							continuous_chunk.append(named_entity)
							current_chunk = []
					else:
						continue
				for i in range(len(continuous_chunk)):
					namedEnt.append(continuous_chunk[i])
			print(namedEnt)
			return namedEnt
		except Exception as e:
			print(str(e))
