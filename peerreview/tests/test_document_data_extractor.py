"""
@author: Emmanuele Abbenante <emmanuele.abbenante@gmail.com>
"""
import pytest

from peerreview.texts.document_data_extractor import MimeNotValidError, \
    DocumentType, DOCXExtractionStrategy, PDFExtractionStrategy


class TestDocumentType:
    
    def test_get_document_type_instance_with_not_valid_mime(self):
        # Arrange
        mime_type = 'application/json'
        
        # Act/Assert
        with pytest.raises(MimeNotValidError):
            DocumentType.get_instance(mime_type)

    def test_get_document_type_instance_with_valid_mime(self):
        # Arrange
        mime_type = 'application/pdf'
        expected_doc_type = DocumentType.PDF
        
        # Act
        actual_doc_type = DocumentType.get_instance(mime_type)
        
        # Assert
        assert expected_doc_type == actual_doc_type


class TestDOCXExtractionStrategy:
    
    def test_extract_document_data(self):
        # Arrange
        file_path = 'peerreview/tests/example.docx'
        expected_doc_text = 'Indice generale Capitolo 1 2 Paragrafo 1 2 Sottoparagrafo 1 2 Capitolo 2 2 Capitolo 1 Paragrafo 1 Sottoparagrafo 1 Test 1 Capitolo 2 Test 2'
        
        # Act
        actual_doc_data = DOCXExtractionStrategy.extract_document_data(file_path)
        actual_doc_text = actual_doc_data.document_text
        
        # Assert
        assert expected_doc_text == actual_doc_text


class TestPDFExtractionStrategy:
    
    def test_extract_document_data(self):
        # Arrange
        file_path = 'peerreview/tests/example.pdf'
        expected_doc_text = 'Indice generale Capitolo 1.............................................................................................................................................2 Paragrafo 1.......................................................................................................................................2 Sottoparagrafo 1..........................................................................................................................2 Capitolo 2.............................................................................................................................................2  Capitolo 1  Paragrafo 1  Sottoparagrafo 1  Test 1  Capitolo 2  Test 2 '
        expected_toc_json = '[{"Capitolo 1": [{"Paragrafo 1": ["Sottoparagrafo 1"]}]}, "Capitolo 2"]'
        
        # Act
        actual_doc_data = PDFExtractionStrategy.extract_document_data(file_path)
        actual_doc_text = actual_doc_data.document_text
        actual_toc_json = actual_doc_data.toc_as_json()
        
        # Assert
        assert expected_doc_text == actual_doc_text
        assert expected_toc_json == actual_toc_json
    
    def test_from_bytestring_bytestring(self):
        # Arrange
        byte_string = '\u0074\u0065\u0073\u0074'
        expected_string = 'test'
        
        # Act
        actual_string = PDFExtractionStrategy.from_bytestring(byte_string)
        
        # Assert
        assert  expected_string == actual_string

    def test_from_bytestring_string(self):
        # Arrange
        string = 'test'
        expected_string = 'test'
        
        # Act
        actual_string = PDFExtractionStrategy.from_bytestring(string)
        
        # Assert
        assert  expected_string == actual_string

    def test_build_toc_tree_multiple_levels_and_last_item_at_first_level(self):
        # Arrange
        toc_items = [(1, 'Capitolo 1'), (2, 'Paragrafo 1'), (3, 'Sottoparagrafo 1'), (1, 'Capitolo 2')]
        expected_toc_tree = [{'Capitolo 1': [{'Paragrafo 1': ['Sottoparagrafo 1']}]}, 'Capitolo 2']
        
        # Act
        actual_toc_tree = PDFExtractionStrategy.build_toc_tree(toc_items)
        
        # Assert
        assert  expected_toc_tree == actual_toc_tree

    def test_build_toc_tree_two_levels(self):
        # Arrange
        toc_items = [(1, 'Capitolo 1'), (2, 'Paragrafo 1'), (2, 'Paragrafo 2')]
        expected_toc_tree = [{'Capitolo 1': ['Paragrafo 1', 'Paragrafo 2']}]
        
        # Act
        actual_toc_tree = PDFExtractionStrategy.build_toc_tree(toc_items)
        
        # Assert
        assert  expected_toc_tree == actual_toc_tree

    def test_build_toc_tree_one_item(self):
        # Arrange
        toc_items = [(1, 'Capitolo 1')]
        expected_toc_tree = ['Capitolo 1']
        
        # Act
        actual_toc_tree = PDFExtractionStrategy.build_toc_tree(toc_items)
        
        # Assert
        assert  expected_toc_tree == actual_toc_tree

    def test_build_toc_tree_empty_toc(self):
        # Arrange
        toc_items = []
        expected_toc_tree = []
        
        # Act
        actual_toc_tree = PDFExtractionStrategy.build_toc_tree(toc_items)
        
        # Assert
        assert  expected_toc_tree == actual_toc_tree

    def test_build_toc_tree_multiple_toc_subtrees(self):
        # Arrange
        toc_items = [(1, 'Capitolo 1'), (2, 'Paragrafo 1'), (3, 'Sottoparagrafo 1'), (1, 'Capitolo 2'), (2, 'Paragrafo 1'), (2, 'Paragrafo 2'), (3, 'Sottoparagrafo 1'), (1, 'Capitolo 3')]
        expected_toc_tree = [{'Capitolo 1': [{'Paragrafo 1': ['Sottoparagrafo 1']}]}, {'Capitolo 2': ['Paragrafo 1', {'Paragrafo 2': ['Sottoparagrafo 1']}]}, 'Capitolo 3']
        
        # Act
        actual_toc_tree = PDFExtractionStrategy.build_toc_tree(toc_items)
        
        # Assert
        assert  expected_toc_tree == actual_toc_tree

